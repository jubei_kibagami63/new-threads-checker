import bs4
import requests
import time
import datetime


class NewThreadsChecker:
    def __init__(self, chat_id):
        self.__timeout = None
        self.__boards_list = None
        self.__thread_url_template = "https://2ch.hk/{}/res/{}.html"
        self.__board_page_template = "https://2ch.hk/{}/{}.html"
        self.__stop = False
        self.__chat_id = chat_id

    def __substract_minutes(self, date_time, minutes):
        total_seconds = date_time.timestamp()
        total_seconds -= minutes*60
        return datetime.datetime.fromtimestamp(total_seconds)

    def set_timeout(self, timeout):
        self.__timeout = timeout

    def set_boards_list(self, boards_list):
        self.__boards_list = boards_list

    def __get_sub_tag(self, tag, attribute, value):
        return tag.find_all(lambda x: x.get(attribute) == value)

    def __navigate_to_sub_tag(self, tag, attributes_values):
        if not attributes_values:
            raise Exception("Navigation failed!")
        if len(attributes_values) == 1:
            stop_attr, stop_value = attributes_values[0]
            if tag.get(stop_attr) == stop_value:
                return tag
            else:
                raise Exception("Navigation failed!")
        attributes_values = attributes_values[1:]
        attribute, value = attributes_values[0]
        tag = self.__get_sub_tag(tag, attribute, value)[0]
        return self.__navigate_to_sub_tag(tag, attributes_values)

    def __get_thread_date(self, thread):
        id = self.__get_thread_id(thread)
        path_to_thread_date = [
            ("id", "thread-{}".format(id)),
            ("id", "post-{}".format(id)),
            ("id", "post-body-{}".format(id)),
            ("id", "post-details-{}".format(id)),
            ("class", ["post__detailpart"]),
            ("class", ["post__time"])
        ]
        thread_date_string = self.__navigate_to_sub_tag(thread, path_to_thread_date).string
        date, _, time = thread_date_string.split()
        day, month, year = map(int, date.split("/"))
        hour, minute, second = map(int, time.split(":"))
        thread_date = datetime.datetime(2000 + year, month, day, hour, minute, second, 0)
        return thread_date

    def __get_thread_id(self, thread):
        return thread.get("id")[7:]


    def __get_thread_url(self, thread_id, board):
        return self.__thread_url_template.format(board, thread_id)


    def __is_new_thread(self, thread, current_date_time):
        return self.__get_thread_date(thread) > current_date_time

    def __check_new_threads(self, current_date_time, board, message_sender):
        new_thread_ids = self.__get_new_thread_ids(current_date_time, board)
        if new_thread_ids:
            new_thread_urls = [self.__get_thread_url(thread_id, board) for thread_id in new_thread_ids]
            result = "New threads found on board {}:\n{}".format(board, "\n".join(new_thread_urls))
            print("{}: {}".format(self.__chat_id, result))
            message_sender(result)
        else:
            print("{}: No new threads on board {}".format(self.__chat_id, board))

    def __get_new_thread_ids(self, current_date_time, board):
        new_thread_ids = []
        i = 1
        while True:
            try:
                response = requests.get(self.__board_page_template.format(board, i))
            except Exception as e:
                print("{}: Unexpected exception occured: {}".format(self.__chat_id, e))
                break
            if response.status_code != requests.codes.ok:
                break
            source = response.text
            soup = bs4.BeautifulSoup(source, 'lxml')
            if "Архив" in soup.head.title.string:
                break
            threads = self.__get_sub_tag(soup, "class", ["thread"])
            new_thread_ids.extend(
                self.__get_thread_id(thread) for thread in threads if self.__is_new_thread(thread, current_date_time))
            i += 1
            #print("Sleeping 5 sec...")
            #time.sleep(5)
        return new_thread_ids

    def stop(self):
        self.__stop = True

    def run(self, message_sender):
        self.__stop = False
        current_date_time = self.__substract_minutes(
            datetime.datetime.now(), int(self.__timeout))
        while not self.__stop:
            for board in self.__boards_list:
                self.__check_new_threads(current_date_time, board, message_sender)
            print("{}: Sleep for {} minutes...".format(self.__chat_id, self.__timeout))
            sleep_time = 0
            while(sleep_time != int(self.__timeout)*60 and not self.__stop):
                time.sleep(1)
                sleep_time += 1
            current_date_time = self.__substract_minutes(
                datetime.datetime.now(), int(self.__timeout))
