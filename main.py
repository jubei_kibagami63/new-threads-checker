import NewThreadsChecker as ntc
from telegram.ext import Updater, CommandHandler
from threading import Thread


TOKEN = ""

"""
if __name__ == "__main__":
    checker = ntc.NewThreadsChecker(2, ["b", "pa", "math"])
    checker.run()
"""

CheckerThreadPool = {}

def start(update, context):
    update.message.reply_text(
        """
        Commands:
        /run [timeout(in minutes)] [boards_list], e.g. /run 2 b;sex;math;pa
        /stop
        """)

def get_message_sender(update):
    def sender(text):
        update.message.reply_text(text)
    return sender


def run(update, context):
    user_message = update.message.text
    _, timeout, boards = user_message.split()
    boards_list = boards.split(";")
    chat_id = update.message.chat_id
    message_sender = get_message_sender(update)
    checker = ntc.NewThreadsChecker(chat_id)

    def run_checker():
        checker.set_timeout(timeout)
        checker.set_boards_list(boards_list)
        checker.run(message_sender)

    CheckerThreadPool[chat_id] = {
        "thread": Thread(target=run_checker),
        "checker": checker
    }
    CheckerThreadPool[chat_id]["thread"].start()
    update.message.reply_text("Bot running...")
    print("{}: Bot running...".format(chat_id))


def stop(update, context):
    chat_id = update.message.chat_id
    checker = CheckerThreadPool[chat_id]["checker"]
    checker_thread = CheckerThreadPool[chat_id]["thread"]
    update.message.reply_text("Stopping bot...")
    print("{}: Stopping bot...".format(chat_id))
    checker.stop()
    checker_thread.join()
    update.message.reply_text("Bot stopped.")
    print("{}: Bot stopped.".format(chat_id))

def main():
    updater = Updater(TOKEN, use_context=True)
    dp = updater.dispatcher
    dp.add_handler(CommandHandler("start", start))
    dp.add_handler(CommandHandler("run", run))
    dp.add_handler(CommandHandler("stop", stop))
    updater.start_polling()
    updater.idle()


if __name__ == "__main__":
    main()
